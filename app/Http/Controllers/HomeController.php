<?php

namespace App\Http\Controllers;

use http\Message;
use Mail;
use Illuminate\Http\Request;
use App\Contact;

class HomeController extends Controller
{
    /**
     * GET /
     * Display Homepage
     */
    public function index()
    {
        return view('home.index');
    }

    /**
     * POST /contact
     * Process the contact form
     */
    public function processMessage(Request $request)
    {
        #Validate the request data
        $request->validate([
            'full_name' => 'required|max:255',
            'email' => 'required|email',
            'message' => 'required',
        ]);

        $name = $request->full_name;
        $email = $request->email;
        $message = $request->message;
        $phone = $request->phone;

        $contact = new Contact();
        $contact->full_name = $name;
        $contact->email = $email;
        $contact->message = $message;
        $contact->phone = $phone;
        $contact->save();

        $user = 'guy-smiley@example.com';

        Mail::send('home.email', ['name' => $name, 'email' => $email, 'content' => $message, 'phone' => $phone], function ($m) use ($user) {
            $m->from('kenny@appworkstudio.com', 'Contact Message ');

            $m->to($user, 'Kenny')->subject('New Contact Message!');
        });

        return redirect('/')->with(['alert' => 'The message was successfully submitted']);

    }
}

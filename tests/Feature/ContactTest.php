<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ContactTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testExample()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    public function name_field_test()
    {
        $response = $this->post('/contact', [
            'full_name'=> str_repeat('a', 50),
            'email' => str_repeat('a', 247).'@test.com',
            'message' => 'this is the message',
            'phone' => '1234567890'
        ]);

        $response->assertStatus(302);
        $response->assertSessionHasErrors([
            'full_name' => 'The name field may not be greater than 255 characters.'
        ]);
    }

    function email_validation_should_reject_invalid_emails()
    {
        collect(['you@example,com', 'bad_user.org', 'example@bad+user.com'])->each(function ($invalidEmail) {
            $this->post('/users', [
                'name' => str_repeat('a', 50),
                'email' => $invalidEmail,
                'message' => 'this is the message',
                'phone' => '1234567890'
            ])->assertSessionHasErrors([
                'email' => 'The email must be a valid email address.'
            ]);
        });
    }
}

<h1>Message Details</h1>
<p><strong>Full Name:</strong> {{ $name }}</p>
<p> <strong>Email:</strong> {{ $email }}</p>
<p><strong>Message:</strong> {{ $content }}</p>
@if($phone)
    <p><strong>Phone:</strong> {{ $phone }}</p>
@endif


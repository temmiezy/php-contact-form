# Dealer Inspire PHP Code Challenge by Kenny Afolabi

Welcome to the Dealer Inspire PHP Code challenge. 

## About

This is a laravel built project for a developer position at Dealer Inspire

## About App
- Simple Contact Form with a form submission
- Migration file feature to create tables
- form validation
- data submission to database
- email notification on submission
- Unit testing on form fields

## To Test
run -> php artisan serve

